import re


_LOG_FILE = '/tmp/querylog'
_QUERY_OUTPUT_FILE = '/tmp/queries.html'
_IP_OUTPUT_FILE_PREFIX = '/tmp/ip_'
_QUERY_REGEX = r'client ([^#]+).+query: ([^ ]+)' 

_HTML_HEADER = '<html><head></head><body><table><tr><th>Query</th><th>Count</th></tr>'
_HTML_FOOTER = '</table></body></html>'


def _write_table_from_dict(d, output_file):
    i = 0
    html = _HTML_HEADER
    for key, value in sorted(d.iteritems(), key=lambda (k, v): (v, k), reverse=True):
        if i == 500:
            break
        html += '<tr><td>' + key + '</td><td>' + str(value) + '</td></tr>'
        i += 1
    html += _HTML_FOOTER
    with open(output_file, 'w') as f:
        f.write(html)


def main():
    with open(_LOG_FILE) as f:
        log = f.read()

    log = log.split('\n')

    regex = re.compile(_QUERY_REGEX)

    queries = dict()
    ips = dict()
    for query in log:
        match = regex.search(query)
        if not match or not match.group(1) or not match.group(2):
            continue
        ip = match.group(1)
        host = match.group(2)
        queries_host = queries.get(host)
        if not queries_host:
            queries[host] = 1
        else:
            queries[host] += 1
        client = ips.get(ip)
        if not client:
            ips[ip] = {host: 1}
        else:
            client_host = ips[ip].get(host)
            if not client_host:
                ips[ip][host] = 1
            else:
                ips[ip][host] += 1

    _write_table_from_dict(queries, _QUERY_OUTPUT_FILE)

    for ip in ips.keys():
        _write_table_from_dict(ips[ip], _IP_OUTPUT_FILE_PREFIX + str(ip) + '.html')


if __name__ == '__main__':
    main()
